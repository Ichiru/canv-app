import React, { useState, useEffect } from 'react';
import './App.css';
import { Canvas } from './Canvas';
import { getText } from '../../Api/inputApi';

export const App = () => {
    const [taskList, setTaskList] = useState([]);
    const [isFetched, setIsFetched] = useState(false);
    const [cells, setCells] = useState([]);

    const handleSetTaskList = taskList => {
        setTaskList(taskList);
    };

    const handleSetCells = cells => {
        setCells(cells);
    };

    const parseResponse = str => {
        return str.split('\n');
    };

    const assignCells = (width, height) => {
        const cells = [];
        for (let y = 1; y < height + 1; y++) {
            const row = [];
            for (let x = 1; x < width + 1; x++) {
                row.push({
                    id: '' + y + x,
                    cords: {
                        y: y,
                        x: x
                    },
                    isMarked: false,
                    color: ''
                });
            }
            cells.push(row);
        }
        handleSetCells(cells);
    };

    const initCanvas = taskList => {
        const params = taskList.filter(task => {
            return task.split(' ')[0] === 'C';
        });

        if (!params[0]) {
            throw new Error('Canvas in not initialised');
        }

        const param = params[0].split(' ');

        if (
            Number(param[1]) < 1 ||
            Number(param[2]) < 1 ||
            Number(param[1]) > 60 ||
            Number(param[2]) > 60
        ) {
            throw new Error('Unavailable size of the canvas');
        }

        assignCells(Number(param[1]), Number(param[2]));
    };

    const cutTaskList = taskList => {
        const params = taskList.find(task => {
            return task.split(' ')[0] === 'C';
        });
        const index = taskList.indexOf(params);

        handleSetTaskList(taskList.slice(index));
    };

    useEffect(() => {
        getText('http://localhost:5000/')
            .then(res => {
                return parseResponse(res);
            })
            .then(taskList => {
                cutTaskList(taskList);
                initCanvas(taskList);
                setIsFetched(true);
            });
    }, []);

    console.log('ffs');

    return (
        <div className='app'>
            {isFetched && <Canvas cells={cells} taskList={taskList} />}
        </div>
    );
};
