import React from 'react';
import styles from './Canvas.css';
import classNames from 'classnames';
import { Cell } from './Cell';

const cx = classNames.bind(styles);

export const Canvas = ({ cells, taskList }) => {
    const createCanvas = () => {
        return cells.map((row, index) => (
            <div className={cx('canvas__row')} key={index}>
                {row.map(item => (
                    <Cell
                        isMarked={item.isMarked}
                        color={item.color}
                        key={item.id}
                    />
                ))}
            </div>
        ));
    };

    const drawLine = (x1, y1, x2, y2) => {
        if (y1 === y2 || x1 === x2) {
            cells.forEach(row => {
                row.forEach(item => {
                    if (
                        item.cords.x >= x1 &&
                        item.cords.x <= x2 &&
                        item.cords.y >= y1 &&
                        item.cords.y <= y2
                    ) {
                        item.isMarked = true;
                    }
                });
            });
        }
    };

    const drawRectangle = (x1, y1, x2, y2) => {
        drawLine(x1, y1, x2, y1);
        drawLine(x1, y2, x2, y2);
        drawLine(x1, y1, x1, y2);
        drawLine(x2, y1, x2, y2);
    };

    const bucketFill = (x, y, color) => {
        if (!cells[y] || !cells[y][x]) {
            return 0;
        }
        if (!cells[y][x].isMarked) {
            cells[y][x].color = color;

            if (
                cells[y + 1] &&
                !cells[y + 1][x].isMarked &&
                cells[y + 1][x].color !== color
            ) {
                bucketFill(x, y + 1, color);
            }

            if (
                cells[y][x + 1] &&
                !cells[y][x + 1].isMarked &&
                cells[y][x + 1].color !== color
            ) {
                bucketFill(x + 1, y, color);
            }

            if (
                cells[y][x - 1] &&
                !cells[y][x - 1].isMarked &&
                cells[y][x - 1].color !== color
            ) {
                bucketFill(x - 1, y, color);
            }

            if (
                cells[y - 1] &&
                !cells[y - 1][x].isMarked &&
                cells[y - 1][x].color !== color
            ) {
                bucketFill(x, y - 1, color);
            }
        }
    };

    taskList.forEach(item => {
        let arr = item.split(' ');
        if (arr[0] === 'L') {
            drawLine(
                Number(arr[1]),
                Number(arr[2]),
                Number(arr[3]),
                Number(arr[4])
            );
        }
        if (arr[0] === 'R') {
            drawRectangle(
                Number(arr[1]),
                Number(arr[2]),
                Number(arr[3]),
                Number(arr[4])
            );
        }
        if (arr[0] === 'B') {
            bucketFill(Number(arr[1]), Number(arr[2]), arr[3]);
        }
    });

    return <div className={cx('canvas')}>{createCanvas()}</div>;
};
