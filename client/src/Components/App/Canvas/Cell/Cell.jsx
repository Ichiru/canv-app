import React from 'react';
import styles from './Cell.css';
import classNames from 'classnames';

const cx = classNames.bind(styles);

export const Cell = ({ isMarked, color }) => {
    return (
        <div className={cx('cell')}>
            {isMarked && <div className='cell__marked'>&#120;</div>}
            {color !== '' && <div className='cell__marked'>{color}</div>}
        </div>
    );
};
