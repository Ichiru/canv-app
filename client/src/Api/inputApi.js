export const getText = async url => {
    let response = await fetch(url);

    if (response.ok) {
        return await response.text();
    } else {
        alert('HTTP error: ' + response.status);
    }
};
